set -eou pipefail 

symlink_with_bak "$DOT_FILES_BASE/mac/.bash_profile" "$HOME/.bash_profile"
symlink_with_bak "$DOT_FILES_BASE/mac/.bashrc" "$HOME/.bashrc"

# TODO(ecarlin): Switch ctrl and alt modifier keys
# https://apple.stackexchange.com/questions/13598/updating-modifier-key-mappings-through-defaults-command-tool
# Keyboard - enable hold down key to repeat
defaults write NSGlobalDomain ApplePressAndHoldEnabled -bool false
# Keyboard - set a blazingly fast key repeat rate
defaults write NSGlobalDomain KeyRepeat -int 1
defaults write NSGlobalDomain InitialKeyRepeat -int 10
# Finder - how all filename extensions
defaults write NSGlobalDomain AppleShowAllExtensions -bool true
# Finder - show hidden files (not so hidden after all)
defaults write com.apple.finder AppleShowAllFiles YES
# Finder - show path bar
defaults write com.apple.finder ShowPathbar -bool true
# Finder - when performing a search, search the current folder by default
defaults write com.apple.finder FXDefaultSearchScope -string "SCcf"
# OS - disable resume system-wide
defaults write com.apple.systempreferences NSQuitAlwaysKeepsWindows -bool false

# Install homebrew
install_if_command_not_found "brew" "/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)""
# Install ack (a faster grep)
install_if_command_not_found "ack" "brew install ack"
# Install VS Code
install_if_command_not_found "code" "brew cask install visual-studio-code"

# Symlink vscode settings
readonly VSCODE_SETTINGS="${HOME}/Library/Application Support/Code/User"
symlink_with_bak "${VSCODE_BASE}/snippets" "${VSCODE_SETTINGS}/snippets"
symlink_with_bak "${VSCODE_BASE}/keybindings.json" "${VSCODE_SETTINGS}/keybindings.json"
symlink_with_bak "${VSCODE_BASE}/settings.json" "${VSCODE_SETTINGS}/settings.json"

#TODO(e-carlin): Figure out how to get this to bubble up so I don't have to manually run it
echo "Now run 'source ~/.bash_profile'"
