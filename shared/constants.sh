export readonly DOT_FILES_BASE="${HOME}/code/dotfiles"
export readonly VSCODE_BASE="${DOT_FILES_BASE}/shared/VSCode"

symlink_with_bak() {
    if [[ -f "$2" || -d "$2" ]]; then
        if [[ -f "$2.bak" || -d "$2.bak" ]]; then
            rm -rf "$2.bak"
        fi
        mv "$2" "$2.bak"
    fi
    ln -s "$1" "$2"
}
export -f symlink_with_bak

install_if_command_not_found() {
    command -v $1 >/dev/null 2>&1  || $2
}
export -f install_if_command_not_found
