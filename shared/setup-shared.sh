set -ou pipefail # no set -e read has non-zero exit code so it fails script
                 # TODO(ecarlin): Why?

echo "Settings up shared configs"

source "${HOME}/code/dotfiles/shared/constants.sh"

symlink_with_bak "$DOT_FILES_BASE/shared/.bash_aliases" "$HOME/.bash_aliases"
symlink_with_bak "$DOT_FILES_BASE/shared/.vimrc" "$HOME/.vimrc"
symlink_with_bak "$DOT_FILES_BASE/shared/.tmux.conf" "$HOME/.tmux.conf"
symlink_with_bak "$DOT_FILES_BASE/shared/.inputrc" "$HOME/.inputrc"
symlink_with_bak "$DOT_FILES_BASE/shared/.vim" "$HOME/.vim"
symlink_with_bak "$DOT_FILES_BASE/shared/.gitconfig" "$HOME/.gitconfig"

# Create ~/bin to put my specific scripts in
mkdir -p "$HOME/bin"

# Do OS specific install
readonly OS="$(uname -a | cut -d ' ' -f1)"
if [[ "${OS}" = "Linux" ]]; then
    echo "Linux OS identified. Proceeding with Linux specific setup"
    bash "${DOT_FILES_BASE}/linux/setup-linux.sh"
elif [[ "${OS}" = "Darwin" ]]; then
    echo "Darwin OS identified. Proceeding with Darwin specific setup"
    bash "${DOT_FILES_BASE}/mac/setup-mac.sh"
else
    "OS ${OS} is unsupported. Exiting..."
    exit 1
fi

# Install VSCode extensions
read -r -d '' DESIRED_VSCODE_EXTENSIONS <<'EOF'
CoenraadS.bracket-pair-colorizer
eamodio.gitlens
esbenp.prettier-vscode
formulahendry.auto-close-tag
formulahendry.code-runner
ms-vscode.cpptools
ms-vscode-remote.remote-containers
streetsidesoftware.code-spell-checker
redhat.java
vscodevim.vim
vscjava.vscode-java-debug
vscjava.vscode-java-pack
ms-vscode.vscode-typescript-tslint-plugin
EOF
DESIRED_VSCODE_EXTENSIONS="$(echo "$DESIRED_VSCODE_EXTENSIONS" | sort)"

INSTALLED_VSCODE_EXTENSIONS="$(code --list-extensions | sort)"
VSCODE_EXTENSIONS_TO_UNINSTALL="$(echo $(diff --unchanged-line-format= --old-line-format= --new-line-format='%L' <(echo "$DESIRED_VSCODE_EXTENSIONS") <(echo "$INSTALLED_VSCODE_EXTENSIONS")) | sort)"
VSCODE_EXTENSIONS_TO_INSTALL="$(diff --unchanged-line-format= --old-line-format= --new-line-format='%L' <(echo "$INSTALLED_VSCODE_EXTENSIONS") <(echo "$DESIRED_VSCODE_EXTENSIONS"))"

[[ ! -z "$VSCODE_EXTENSIONS_TO_UNINSTALL" ]] && echo "Uinstalling VSCode extesnions: ${VSCODE_EXTENSIONS_TO_UNINSTALL}" \
&& echo "$VSCODE_EXTENSIONS_TO_UNINSTALL" | xargs -n 1 code --uninstall-extension

[[ ! -z "$VSCODE_EXTENSIONS_TO_INSTALL" ]] && echo "Installing VSCode extesnions: ${VSCODE_EXTENSIONS_TO_INSTALL}" \
&& echo "$VSCODE_EXTENSIONS_TO_INSTALL" | xargs -n 1 code --install-extension
