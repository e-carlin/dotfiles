set -eou pipefail

# TODO(e-carlin): The default ubuntu vim doesn't come with clipboard. I've added
# support for this with `sudo apt remove vim && sudo apt install vim-gtk`. Add
# to this script.

# TODO(e-carlin): alt+tab default behavior is to groups windows of same app
# To change keyboard shortcut so it doesn't group follow along with
# https://askubuntu.com/a/996121/983450 . Find a way to do that in this script

install_code_ubuntu() {
    install_if_command_not_found "code" "sudo snap install code --classic"
}

echo "Setting up Linux env"

# Add sourcing of custom .bashrc to existing .bashrc
if grep -xq "source $HOME\/code\/\dotfiles\/linux\/\.bashrc" ~/.bashrc
then
    echo "Sourcing of my custom .bashrc already present in system .bashrc. Not adding."
else
    echo "Adding sourcing of my custom .bashrc to system .bashrc"
    echo -e "\n\n# My custom .bashrc" >> ~/.bashrc
    echo "source ${DOT_FILES_BASE}/linux/.bashrc" >> ~/.bashrc
fi

# Add script to center window
cp "$DOT_FILES_BASE/bin/center-window" "$HOME/bin"

readonly IS_UBUNTU="$(cat /etc/os-release | grep -q "Ubuntu")"
echo $IS_UBUNTU
if grep -q "Ubuntu" "/etc/os-release"; then
    echo "Ubuntu distro identified"
    install_code_ubuntu
else
    echo "Linux distro not recognized. Ubuntu is the only distro currently supported. Exiting..."
    exit 1
fi
# if [[ cat /etc/os-release | grep "Ubuntu" ]]


# Symlink vscode settings
readonly VSCODE_SETTINGS="${HOME}/.config/Code/User"
symlink_with_bak "${VSCODE_BASE}/snippets" "${VSCODE_SETTINGS}/snippets"
symlink_with_bak "${VSCODE_BASE}/keybindings.json" "${VSCODE_SETTINGS}/keybindings.json"
symlink_with_bak "${VSCODE_BASE}/settings.json" "${VSCODE_SETTINGS}/settings.json"


echo "Now run source ~/.bashrc"
