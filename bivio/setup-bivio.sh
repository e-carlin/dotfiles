echo "Setting up bivio machine"

source "${HOME}/code/dotfiles/shared/constants.sh"
symlink_with_bak "${DOT_FILES_BASE}/shared/.bash_aliases" "$HOME/.bash_aliases"
symlink_with_bak "$DOT_FILES_BASE/bivio/.post_bivio_bashrc" "$HOME/.post_bivio_bashrc"
symlink_with_bak "$DOT_FILES_BASE/bivio/.pre_bivio_bashrc" "$HOME/.pre_bivio_bashrc"
symlink_with_bak "$DOT_FILES_BASE/shared/.vimrc" "$HOME/.vimrc"
symlink_with_bak "$DOT_FILES_BASE/shared/.tmux.conf" "$HOME/.tmux.conf"
symlink_with_bak "$DOT_FILES_BASE/shared/.inputrc" "$HOME/.inputrc"
symlink_with_bak "$DOT_FILES_BASE/shared/.vim" "$HOME/.vim"

# Symlink vscode settings
readonly VSCODE_SETTINGS="${HOME}/Library/Application Support/Code/User"
symlink_with_bak "${VSCODE_BASE}/snippets" "${VSCODE_SETTINGS}/snippets"
symlink_with_bak "${VSCODE_BASE}/keybindings.json" "${VSCODE_SETTINGS}/keybindings.json"
symlink_with_bak "${VSCODE_BASE}/settings.json" "${VSCODE_SETTINGS}/settings.json"

# Do OS specific install
readonly OS="$(uname -a | cut -d ' ' -f1)"
if [[ "${OS}" = "Darwin" ]]; then
    echo "Darwin OS identified. Proceeding with Darwin specific setup"

# Install VSCode extensions
    read -r -d '' DESIRED_VSCODE_EXTENSIONS <<'EOF'
CoenraadS.bracket-pair-colorizer
eamodio.gitlens
ms-vscode-remote.remote-containers
streetsidesoftware.code-spell-checker
vscodevim.vim
ms-vscode-remote.vscode-remote-extensionpack
ms-vscode-remote.remote-ssh
EOF
    DESIRED_VSCODE_EXTENSIONS="$(echo "$DESIRED_VSCODE_EXTENSIONS" | sort)"

    INSTALLED_VSCODE_EXTENSIONS="$(code --list-extensions | sort)"
    VSCODE_EXTENSIONS_TO_UNINSTALL="$(echo $(diff --unchanged-line-format= --old-line-format= --new-line-format='%L' <(echo "$DESIRED_VSCODE_EXTENSIONS") <(echo "$INSTALLED_VSCODE_EXTENSIONS")) | sort)"
    VSCODE_EXTENSIONS_TO_INSTALL="$(diff --unchanged-line-format= --old-line-format= --new-line-format='%L' <(echo "$INSTALLED_VSCODE_EXTENSIONS") <(echo "$DESIRED_VSCODE_EXTENSIONS"))"

    [[ ! -z "$VSCODE_EXTENSIONS_TO_UNINSTALL" ]] && echo "Uinstalling VSCode extesnions: ${VSCODE_EXTENSIONS_TO_UNINSTALL}" \
    && echo "$VSCODE_EXTENSIONS_TO_UNINSTALL" | xargs -n 1 code --uninstall-extension

    [[ ! -z "$VSCODE_EXTENSIONS_TO_INSTALL" ]] && echo "Installing VSCode extesnions: ${VSCODE_EXTENSIONS_TO_INSTALL}" \
    && echo "$VSCODE_EXTENSIONS_TO_INSTALL" | xargs -n 1 code --install-extension
fi
