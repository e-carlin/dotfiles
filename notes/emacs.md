- Create folder
`M-x make-directory`
- Exit minibuffer
`C-g`
- Search forward
`C-s search-term`
- Search backward
`C-r search term`
- Kill buffer
`C-x k`
-  Move focus to other window
`C-x o`
- Kill window
`C-x 0`
- Scroll forward word
`C-f`
- undo
`C-x u`
- Go to end of buffer
`M->`
- Go to end of buffer
`M->`
- Harmless command to end things
`C-f`
- Set mark
`C-SPC`
- Kill region
`C-w`
- copy region to kill rind
`M-w`
- Yank back last killed thing
`C-y`
- Move cursor up line
`C-p`
- Move cursor down line
`C-n`
- undo
`C-x u`
- Move cursor forward char
`C-f`
- Move cursor backward char
`C-b`
- Move cursor forward word
`C-f`
- Move cursor backward word
`C-b`
-Show directory tree
`C-x d`
- Save buffer
`C-x s`
- Open shell
`M-x shell`
- Scroll back in shell history
`M-p`
- Scroll forward in shell history
`M-n`
- Open shell
`C-x RET`
- List buffers
`C-x C-b`
- Open buffer list
`C-x C-b`
- In buffer list
`C-k # marks buffer for deletion`
`x # runs deletion`
- Refresh dired
`g`
- Enter dired
`C-x d`
- Mark file for deletion
`d`
- Undo mark file for deletion
`u`
- Execute file deletion
`x`
- Minibuffer kill line
`C-a #move to start of line`
`C-k #kill to end of line`
-Delete backwards one word
`M-BAK`
- Show file name
`C-F1`
-Balance windows
`C-x +`
-Go to previos buffer
`C-x <left-arrow>`
-Go to definition
`f12`
- Go back from definition
`M-,`
- Save window to register
`C-x r w <reg-name>`
- Load window from register
`C-x r j <reg-name>`
- Create second shell
`C-u M-x shell`
- Kill buffer and window
`C-x 4 0`
- Clear buff
`C-c M-o`
- Rename buffer
`M-x rename-buffer`
- comment code with evil-commentary
`gcc`
- change cwd
`M-x cd <path>`
- Cycle through elpy completions
`M-n || M-p`
- enable/disable line numbers
`M-x linum-mode`
