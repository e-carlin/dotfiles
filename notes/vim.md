-Jump to tag
`C-]`
- Go back to previous position
`C-o`
- help for insert mode C-H
`:help i_CTRL-H`
- grep help
`:helpgrep`
- find (search) forward for x in line
`fx`
- repeat find
`;`
- repeat in opposite direction
`,`
- set mps (matching pairs)
`:set mps+=<:>`
- move to top of visible file
`H`
- move to mid of visible file
`M`
- move to end of visible file
`E`
- get file path and location of file
`C-g`
- get infor about current position in file
`:set ruler`
- scroll down half screen
`C-d`
- scroll up half screen
`C-u`
- move cursor to top,mid,bottom
`zt|z|b`
- go back from jump
`\`\``
- change text (ex word)
`cw`
- change word "four" to "five"
`/four<Enter>   #find the first string "four"`
`cwfive<Esc>    #change the word to "five"   `
`n              #find the next "four"        `
`.              #repeat the change to "five" `
`n              #find the next "four"        `
`.              #repeat the change           `
- delete a word with cursor in middel
`daw`
- lookup :help for text-objects
- change case of char under cursor (or visual mode)
`~`
- add to top of vimrc
`source $VIMRUNTIME/defaults.vim`
- see tabs
`:set list
- tab and trailing whitespace chars
`:set listchars=tab:>-,trail:-`
- see color schemes
`$VIMRUNTIME/colors`
`:colorscheme name-of-scheme`
- autowrite file
`:set autowrite`
-file level marks
`m<CAPITAL_LETTER>`
- yank a wor
`yaw`
-rename file
`:saveas`
- quite file
`ZZ`
- change height of window
`C-w +|-`
- even window splits
`C-w=`
- open file under cursor
`gf`
- set working dir for tab
`:tcd /path/to/dir`
- rehighlight visual selection
`gv`
- i'm on usr_08.txt reading about 08.5 moving windows
